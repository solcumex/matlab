%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TAREA ALGORITMO FORWARD,BACKWARD Y VITERBI PARA MODELOS OCULTOS DE MARKOV.
%BAUM-WELCH final
%ALAIN MANZO MARTINEZ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
clc;
format long g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%MODELO INICIAL
%Matriz de transiciones entre estados.

% aij=[.3,.1,.4,.2;.7,.1,.1,.1;.2,.3,.2,.3;.1,.2,.2,.5];    
aij=[0.23   0.07   0.05  0.01  0.01  0.63;
0.05   0.27    0.01  0.01  0.01  0.65;
0.56   0.29    0.01   0.01   0.01  0.12;
0.59   0.37    0.01    0.01   0.01  0.01;
0.45   0.01    0.2      0.01   0.09    0.24;
0.44   0.01    0.01     0.01   0.01    0.52];
%Matriz de prob. de simbolos.
bjk=[0.23   0.07   0.05  0.01  0.01  0.63;
    0.23   0.07   0.05  0.01  0.01  0.63;
    0.23   0.07   0.05  0.01  0.01  0.63;
   0.23   0.07   0.05  0.01  0.01  0.63;
   0.23   0.07   0.05  0.01  0.01  0.63;
    0.23   0.07   0.05  0.01  0.01  0.63];

%Vector de prob. de iniciar en un edo.
% h=[.2;.3;.1;.4];       
h=[0.23   0.07   0.05  0.01  0.01  0.63];
         
%MATRIZ DE SECUENCIAS
ms=[1,1,2,2,3,3,4,4,0,0,0;...%AABBCCDD
    1,2,2,3,2,2,4,4,0,0,0;...%ABBCBBDD
    1,3,2,3,2,3,4,0,0,0,0;...%ACBCBCD
    1,4,0,0,0,0,0,0,0,0,0;...%AD
    1,3,2,3,2,1,2,3,4,4,0;...%ACBCBABCDD
    2,1,2,1,1,4,4,4,0,0,0;...%BABAADDD
    2,1,2,3,4,3,3,0,0,0,0;...%BABCDCC
    1,2,4,2,2,3,3,4,4,0,0;...%ABDBBCCDD
    1,2,1,1,1,3,4,3,3,4,0;...%ABAAACDCCD
    1,2,4,0,0,0,0,0,0,0,0];  %ABD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for u=1:1:5000
sumagama=0;
sumaobser=0;
sumachi=0;
sumaga=0;
[x,y]=size(ms);         %Consigue numero de secuencias.
%Extrae secuencia a procesar
for r=1:1:x
max=y;
for w=1:1:max
    if ms(r,w)==0
      max=w;
    else
      secuencia(w)=ms(r,w);
    end    
end
[N,M]=size(bjk);        %Consigue numero de estados (N).
T=length(secuencia);    %Consigue longitud de la secuencia (T).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calcula algoritmo forward y la probabilidad de que se de la secuencia
%de observación dado el modelo.
%Inicialización
for i=1:1:N
a(i,1)=h(i)*bjk(i,secuencia(1));  
end
%Induccion
A(:,1)=a;
c=0;
for k=2:1:T
for j=1:1:N
    for i=1:1:N
    b(i,1)=a(j,1)*aij(j,i);  
    end
    c=c+b;
end
a=c.*bjk(:,secuencia(k));
A(:,k)=a;
c=0;
end
%Terminación
p=sum(a(:,1));      %Probabilidad de la secuencia de observación
A;                  %Matriz de Probabilidades alfa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calcula algoritmo backward
%Inicialización
for i=1:1:N
B(i,1)=1;
end
%Inducción
BJ(:,T)=B;
for k=T:-1:2
for j=1:1:N
    for i=1:1:N
    bb(i,1)=bjk(i,secuencia(k))*B(i,1)*aij(j,i);  
    end  
    c(j,1)=sum(bb(:));
end
    B=c;
    BJ(:,k-1)=B;
end
%Terminación
pp=0;
for i=1:1:N
    pp=pp+bjk(i,secuencia(1))*h(i)*B(i,1);
end
pp;                 %Probabilidad de la secuencia de observación
BJ;                 %Matriz de Probabilidades beta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculo de la funcion gamma.
for k=1:1:T
    for i=1:1:N
        G(i,k)=A(i,k)*BJ(i,k)/p;
    end
end
G;                  %Matriz gama
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%numero esperado de transiciones en el estado i en todo en tiempo.
%Sumatoria de gama desde 1 hasta T.
C=0;
for i=1:1:N
    for k=1:1:T
        O2(i,1)=C+G(i,k);
        C=O2(i,1);
    end
    C=0;
end
%Suma de la sumatoria de gama. (multiple secuencia)
sumagama=O2+sumagama;   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculo numero esperado de estar en el estado i y ver el simbolo k
C=0;
for i=1:1:N
    for j=1:1:M
        for k=1:1:T
            if secuencia(k)==j
                S1(i,j)=G(i,k)+C;
                C=S1(i,j);
            end
        end
        C=0;
    end
end
sumaobser=S1+sumaobser;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calcula la probabilidad de cambiar del estado i al estado j (E)
l=1;
for k=1:1:T-1
for i=1:1:N
    for j=1:1:N
        E(l,k)=A(i,k)*aij(i,j)*bjk(j,secuencia(k+1)).*BJ(j,k+1)/p;
        l=l+1;
    end
end
l=1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculo de la sumatoria de gama hasta T-1.
C=0;
for i=1:1:N
    for j=1:1:T-1
        O1(i,1)=C+G(i,j);
        C=O1(i,1);
    end
    C=0;
end
sumaga=O1+sumaga;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculo de la sumatoria de E
C=0;
[W,R]=size(E);
for i=1:1:W
    for j=1:1:T-1
        S(i,1)=C+E(i,j);
        C=S(i,1);
    end
    C=0;
end
sumachi=S+sumachi;

clear secuencia;
end                     %final del for demultiple secuencias

%construye matriz bnjk
for i=1:1:N
    for j=1:1:M
        bnjk(i,j)=sumaobser(i,j)/sumagama(i);
    end
end
bjk=bnjk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CALCULO DE aij nuevo
l=1;
for i=1:1:N
    for j=1:1:N
        anij(i,j)=sumachi(l)/sumaga(i);
        l=l+1;
    end
end
aij=anij
hn=[1;0;0;0];
h=hn
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%


secuencia=[1,1,2,2,3,3,4];
[M,N]=size(aij);            %Consigue numero de estados (N).
T=length(secuencia);        %Consigue longitud de la secuencia (T).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO FORWARD
for i=1:1:N
a(i,1)=h(i)*bjk(i,secuencia(1));  %quitar ;% para considerar la prob.
end
A(:,1)=a;
c=0;
for k=2:1:T
for j=1:1:N
    for i=1:1:N
    b(i,1)=a(j,1)*aij(j,i);  
    end
    c=c+b;
end
a=c.*bjk(:,secuencia(k));
A(:,k)=a;
c=0;
end
format long g;
p=sum(a(:,1))
%A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO BACKWARD

for i=1:1:N
B(i,1)=1;
end
BJ(:,T)=B;
for k=T:-1:2
for j=1:1:N
    for i=1:1:N
    bb(i,1)=bjk(i,secuencia(k))*B(i,1)*aij(j,i);  
    end  
    c(j,1)=sum(bb(:));
end
    B=c;
    BJ(:,k-1)=B;
end
pp=0;
for i=1:1:N
    pp=pp+bjk(i,secuencia(1))*h(i)*B(i,1);
end
pp
%BJ

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO VITERBI
j=1;
for i=1:1:N
v(i,1)=h(i)*bjk(i,secuencia(j)); 
end

y=1;
for k=2:1:T
for j=1:1:N
    for i=1:1:N
    b(i,1)=v(i,1)*aij(i,j);  
    end
    b=b*bjk(j,secuencia(k));  
    ref=0;
    for x=1:1:N
        if b(x)>ref
            ref=b(x);
            path1(y,j)=x;
        end
    end
    c(j,1)=ref;
end
    ref=0;
    for x=1:1:N
        if c(x)>ref
            ref=c(x);
            path2(y,1)=x;
        end
    end
    y=y+1;
    v=c;
end
M=length(path2);
z=M+1;
ref=0;
    for x=1:1:N
        if c(x)>ref
            ref=c(x);
            path(z,1)=x;
        end
    end


for i=M:-1:1
    path(i,1)=path1(i,path2(i));
end

path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}

%}
