
secuencia=[1,1,2,2,3,3,4];
[M,N]=size(aij);            %Consigue numero de estados (N).
T=length(secuencia);        %Consigue longitud de la secuencia (T).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO FORWARD
for i=1:1:N
a(i,1)=h(i)*bjk(i,secuencia(1));  %quitar ;% para considerar la prob.
end
A(:,1)=a;
c=0;
for k=2:1:T
for j=1:1:N
    for i=1:1:N
    b(i,1)=a(j,1)*aij(j,i);  
    end
    c=c+b;
end
a=c.*bjk(:,secuencia(k));
A(:,k)=a;
c=0;
end
format long g;
p=sum(a(:,1))
%A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO BACKWARD

for i=1:1:N
B(i,1)=1;
end
BJ(:,T)=B;
for k=T:-1:2
for j=1:1:N
    for i=1:1:N
    bb(i,1)=bjk(i,secuencia(k))*B(i,1)*aij(j,i);  
    end  
    c(j,1)=sum(bb(:));
end
    B=c;
    BJ(:,k-1)=B;
end
pp=0;
for i=1:1:N
    pp=pp+bjk(i,secuencia(1))*h(i)*B(i,1);
end
pp
%BJ

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALGORITMO VITERBI
j=1;
for i=1:1:N
v(i,1)=h(i)*bjk(i,secuencia(j)); 
end

y=1;
for k=2:1:T
for j=1:1:N
    for i=1:1:N
    b(i,1)=v(i,1)*aij(i,j);  
    end
    b=b*bjk(j,secuencia(k));  
    ref=0;
    for x=1:1:N
        if b(x)>ref
            ref=b(x);
            path1(y,j)=x;
        end
    end
    c(j,1)=ref;
end
    ref=0;
    for x=1:1:N
        if c(x)>ref
            ref=c(x);
            path2(y,1)=x;
        end
    end
    y=y+1;
    v=c;
end
M=length(path2);
z=M+1;
ref=0;
    for x=1:1:N
        if c(x)>ref
            ref=c(x);
            path(z,1)=x;
        end
    end


for i=M:-1:1
    path(i,1)=path1(i,path2(i));
end

path