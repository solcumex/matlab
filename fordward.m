function [prob] = fordward(o,bjk,aij,h)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%Algoritmo Forward
%  o=[42,381,42,42,42,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    [N,K]=size(bjk);
    [P,M]=size(o);
    for p=1:1:P
        
    T=M;
        for m=1:1:T
            if o(p,m)==0
                T=m-1;
                break;
            end
        end
    
    
   
    a=zeros(N,T);
    %Inicialización
    for i=1:1:N
        a(i,1)=h(i)*bjk(i,o(1));
    end
    %
    %factor de escalamiento
    c=zeros(1,T);
    suma=0;
    for i=1:1:N
        suma=suma+a(i,1);
    end
    c(p)=1/suma;
    a(:,1)=a(:,1)*c(p);
    %}

    %Inducción
    for t=1:1:T-1
        for j=1:1:N
            suma=0;
            for i=1:1:N
                suma=suma+a(i,t)*aij(i,j);
            end
            a(j,t+1)=suma*bjk(j,o(t+1));
        end
        %
        %factor de escalamiento
        suma=0;
        for i=1:1:N
            suma=suma+a(i,t+1);
        end
        c(t+1)=1/suma;
        a(:,t+1)=a(:,t+1)*c(t+1);
        %}
    end

%{
%terminación normal
p=0;
for i=1:1:N
    p=p+a(i,T);
end
p
%}

%
%Terminación con factor de escalamiento
prod=1;
for i=1:1:T
    prod=prod*c(i);
end
p=1/prod

 
% proba(iii)=p
    end

prob=p;
%}


end

